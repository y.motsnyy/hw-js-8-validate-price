"use strict"; 

const input = document.createElement('input');
const div = document.getElementById('root');

input.placeholder = 'price, $';
input.type = 'number';
input.classList.add('inputStyle');

div.appendChild(input);

input.addEventListener('focus', () => {
  input.classList.add('focusGreen'); 
  input.style.color = 'black';
  input.value = '';

  const spanValid = div.querySelector('span');
  if(spanValid) {
    spanValid.remove();
    input.style.color = 'black';
  }
})

input.addEventListener('blur', () => {
  input.classList.remove('focusGreen'); 

  const value = input.value;
    if (value === '') {
      input.classList.remove('focusGreen'); 
    } else if (value < 0) {
        input.classList.remove('focusGreen'); 
        input.classList.add('nonValid');
        const spanValid = document.createElement('span');
        spanValid.innerText = 'Please enter correct price';
        input.after(spanValid);
    } else {
        input.classList.remove('nonValid');
        const currentPrice = document.createElement('span');
        const button = document.createElement ('button');
        currentPrice.innerText = `Текущая цена: ${value}`;
        currentPrice.classList.add('currentPriceStyle'); 
        button.innerText = 'x';
        currentPrice.appendChild(button);
        button.addEventListener('click', () => removeElement(currentPrice));
        input.style.color = 'green';
        div.before(currentPrice);
        
        function removeElement(elem) {
          elem.remove();
          input.value = '';
      } 
    }
});

